// 给节点装备一个普通碰撞盒
cc.Class({
    extends: cc.Component,

    onLoad() {
        let box = this.node.addComponent(cc.BoxCollider)
        box.size.height = this.node.height + 1
        box.size.width = this.node.width + 1
    },

});
