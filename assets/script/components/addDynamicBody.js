// 给节点装备一个动态类型的刚体和一个物理碰撞盒
cc.Class({
    extends: cc.Component,

    onLoad () {
        let rigidBody = this.node.addComponent(cc.RigidBody)
        rigidBody.type = cc.RigidBodyType.Dynamic

        let blockCollider = this.node.addComponent(cc.PhysicsBoxCollider)
        blockCollider.editing = true
        blockCollider.size.height = this.node.height
        blockCollider.size.width = this.node.width
        blockCollider.apply()
    },

});
