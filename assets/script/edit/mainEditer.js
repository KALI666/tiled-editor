import blockConfig from 'blockConfig'
import { promiseSpriteFrame } from 'imageWrapper'

cc.Class({
    extends: cc.Component,

    properties: {
        blockItem: cc.Prefab
    },

    onLoad() {
        const scene = cc.director.getScene()
        this.canvas = scene.getChildByName('Canvas')
        this.blockItemLayer = this.canvas.getChildByName('sideBar').getChildByName('blcokScrollView').getChildByName('view').getChildByName('content')
        this.blockManager = this.node.getComponent('blockManager')
        this.mapBox = this.canvas.getChildByName('mapBox')
        
        // 关闭调试信息
        cc.debug.setDisplayStats(false)

        // 初始化操作
        this.initMapLayerPosition()
        this.initChooseBlockScroll()
    },

    start() {

    },

    // 初始化地图层的位置  
    initMapLayerPosition() {
        let mapLayer = this.mapBox.getChildByName('mapLayer')
        let mapOriginNode = this.mapBox.getChildByName('mask').getChildByName('sign_left_down')
        let worldPosition = mapOriginNode.parent.convertToWorldSpaceAR(mapOriginNode.getPosition())
        let localPosition = mapLayer.parent.convertToNodeSpaceAR(worldPosition)
        mapLayer.x = localPosition.x
        mapLayer.y = localPosition.y
    },

    // 初始化选择槽
    initChooseBlockScroll() {
        for (let i = 0; i < blockConfig.length; i++) {
            let item = cc.instantiate(this.blockItem)
            let imageNode = item.getChildByName('image')
            let config = blockConfig[i]
            let blockType = config.type
            let imageUrl = config.path

            item.selectId = i
            item.parent = this.blockItemLayer
            item.type = blockType
            promiseSpriteFrame(imageUrl, imageNode)

            item.on(cc.Node.EventType.TOUCH_START, this.blockManager.scrollItemTouchEvent, this.blockManager)
        }

        this.blockItemLayer.getComponent(cc.Layout).updateLayout()
    },

    // update (dt) {},
});
