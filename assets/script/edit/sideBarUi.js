import {
    downloadJsonFile,
    uploadJsonFile
} from 'mapFileWrapper'

const EDITOR_WINDOW_WIDTH = 605 // 地图编辑窗宽度
const EDITOR_WINDOW_HEIGHT = 445 // 地图编辑窗高度

cc.Class({
    extends: cc.Component,

    onLoad() {
        const scene = cc.director.getScene()
        this.canvas = scene.getChildByName('Canvas')
        this.mapBox = this.canvas.getChildByName('mapBox')
        this.sideBar = this.canvas.getChildByName('sideBar')
        this.mapDataManager = this.node.getComponent('mapData')
        this.blockManager = this.node.getComponent('blockManager')

    },

    // 下载地图
    btn_DownloadMap() {
        let mapData = this.mapDataManager.getMapData()
        let content = JSON.stringify(mapData)
        let name = mapData.mapName
        downloadJsonFile(content, name)
    },

    // 上传地图
    btn_UploadMap() {
        uploadJsonFile().then(mapString => {
            this.btn_ClearMapBlocks()
            this.mapDataManager.mapData = JSON.parse(mapString)
            this.blockManager.renderBlocksMap()
        })
    },

    // 清除地图显示
    btn_ClearMapBlocks() {
        this.blockManager.clearBlocksView()
        this.blockManager.clearBlocksData()
    },

    btn_LoadGame() {
        cc.director.loadScene("game")
    },

    // 滑动地图，更改水平视野位置
    sld_HorizontalMove() { // 水平方向移动
        const reserved = 20 // 预留一些位置，为了看到地图边缘，计算太准确的话看不到地图边缘
        let mapLayer = this.mapBox.getChildByName('mapLayer')
        let progress = this.sideBar.getChildByName('sliderLayer').getChildByName('mapLayer_horizontal').getComponent(cc.Slider).progress
        let mapWidth = mapLayer.getChildByName('background').width
        let mapOriginNode = this.mapBox.getChildByName('mask').getChildByName('sign_left_down')
        let worldPosition = mapOriginNode.parent.convertToWorldSpaceAR(mapOriginNode.getPosition())
        let localPosition = mapLayer.parent.convertToNodeSpaceAR(worldPosition)

        if (mapWidth > EDITOR_WINDOW_WIDTH) {
            mapLayer.x = localPosition.x - (mapWidth - EDITOR_WINDOW_WIDTH + reserved) * progress
        }
    },

    // 滑动地图，更改垂直视野位置
    sld_VerticalMove() { // 垂直方向移动
        const reserved = 20 // 预留一些位置，为了看到地图边缘，计算太准确的话看不到地图边缘
        let mapLayer = this.mapBox.getChildByName('mapLayer')
        let progress = this.sideBar.getChildByName('sliderLayer').getChildByName('mapLayer_vertical').getComponent(cc.Slider).progress
        let mapHeight = mapLayer.getChildByName('background').height
        let mapOriginNode = this.mapBox.getChildByName('mask').getChildByName('sign_left_down')
        let worldPosition = mapOriginNode.parent.convertToWorldSpaceAR(mapOriginNode.getPosition())
        let localPosition = mapLayer.parent.convertToNodeSpaceAR(worldPosition)

        if (mapHeight > EDITOR_WINDOW_HEIGHT) {
            mapLayer.y = localPosition.y - (mapHeight - EDITOR_WINDOW_HEIGHT + reserved) * progress
        }
    },

    // 调整视野大小
    sld_FieldView() {
        let mapLayer = this.mapBox.getChildByName('mapLayer')
        let progress = this.sideBar.getChildByName('sliderLayer').getChildByName('fieldView').getComponent(cc.Slider).progress
        let subline = this.mapBox.getChildByName('sublineDrawer')
        mapLayer.scale = progress
        // subline.scale = progress
    },

});
