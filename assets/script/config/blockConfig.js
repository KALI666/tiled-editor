import {
    BLOCK_TYPE,
    RENDER_TYPE
} from 'constant'
 
const config = [
    {
        id: 1,
        name: 'block1',
        type: BLOCK_TYPE.BARRIER,
        path: 'texture/tield/block1',
        width: 40,
        height: 40,
        components: [
            'addStaticBody',
        ]
    },
    {
        id: 2,
        name: 'block2',
        type: BLOCK_TYPE.BARRIER,
        path: 'texture/tield/block2',
        width: 40,
        height: 40,
        components: [
            'addStaticBody',
        ]
    },
    {
        id: 3,
        name: 'block3',
        type: BLOCK_TYPE.BARRIER,
        path: 'texture/tield/block3',
        width: 40,
        height: 40,
        components: [
            'addStaticBody',
        ]
    },
    {
        id: 4,
        name: 'block4',
        type: BLOCK_TYPE.BARRIER,
        path: 'texture/tield/block4',
        width: 40,
        height: 40,
        components: [
            'addStaticBody',
        ]
    },
    {
        id: 5,
        name: 'block5',
        type: BLOCK_TYPE.BARRIER,
        path: 'texture/tield/block5',
        width: 40,
        height: 40,
        components: [
            'addStaticBody',
        ]
    },
    {
        id: 6,
        name: 'block2',
        type: BLOCK_TYPE.BARRIER,
        path: 'texture/tield/block6',
        width: 30,
        height: 66,
        components: [

        ]
    },
    {
        id: 7,
        name: 'oldMan',
        type: BLOCK_TYPE.CHARACTER,
        path: 'texture/tield/oldMan',
        width: 60,
        height: 94,
        components: [
            'addBoxCollider',
            'addDynamicBody',
        ]
    },
    
]
 
export default config
