/**
 * 在带有sprite组件的节点上加载url的图片， 返回promise
 * @param {Sting} url 
 * @param {cc.Node} spriteNode 
 */
const promiseSpriteFrame = (url, spriteNode) => new Promise((resolve, reject) => {
    cc.loader.loadRes(url, cc.SpriteFrame, (err, sprite) => {
        if (err) {
            reject()
        } else {
            spriteNode.getComponent(cc.Sprite).spriteFrame = sprite
            resolve()
        }
    })
})
 
export default {
    promiseSpriteFrame
}
