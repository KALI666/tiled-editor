const TILED_WIDTH = 40 // 每个瓦片的横向距离
const TILED_HEIGHT = 40 // 每个瓦片的纵向距离

/**
 * @param {cc.Vec2} v2Point 传入任意一个坐标，返回该坐标所在栅格的坐标
 */
const pointToTiledPosition = (v2Point) => {
    let gridX = Math.floor(v2Point.x / TILED_WIDTH)
    let gridY = Math.floor(v2Point.y / TILED_HEIGHT)
    let offsetX = 0.5 * TILED_WIDTH
    let offsetY = 0.5 * TILED_HEIGHT
    return cc.v2(gridX * TILED_WIDTH + offsetX, gridY * TILED_HEIGHT + offsetY)
}

/**
 * @param {cc.Vec2} v2Point 传入任意一个坐标，返回该坐标栅格
 */
const poinToGrid = (v2Point) => {
    let gridX = Math.floor(v2Point.x / TILED_WIDTH)
    let gridY = Math.floor(v2Point.y / TILED_HEIGHT)
    return cc.v2(gridX, gridY)
}

export default {
    pointToTiledPosition,
    poinToGrid
}