
// 需要跨工程使用的属性不能用Symbol (比如和地图相关的，要记录到json中的属性)， 只在当前工程使用的属性用Symbol
const BLOCK_TYPE = { // 地图块的类型
    CHARACTER: 'character', // 人物角色
    ENEMY: 'enemy', // 敌人
    BARRIER: 'barrier', // 障碍物
    FUNCTION: 'function', // 功能块
    COLLECTION: 'collection', // 可收集的物品
    
}

const EDITOR_STATE = { // 编辑状态
    READY_ADD_BLOCK: Symbol(), // 准备添加块 
    CLEAN_BLOCK: Symbol(), // 清理块
    COMMON_STATE: Symbol(), // 普通状态
    CHOOSING_BLOCK: Symbol(), // 正选中某个块
}

export default {
    BLOCK_TYPE,
    EDITOR_STATE
}
