
// 键盘码转键盘符
const codeToKey = (code) => {
    code += ''
    let table = {
        '65': 'a',
        '68': 'd',
        '74': 'j'
    }
    return table[code]
}

cc.Class({
    extends: cc.Component,

    onLoad() {
        let scene = cc.director.getScene()
        this.canvas = scene.getChildByName('Canvas')
        this.player = null
        this.horizontalSpeed = 200
        this.verticalSpeed = 300

        this.isStop = true
        this.isLeft = false

        this.initKeyEventListen()
    },

    start() {

    },

    // 初始化键盘事件监听
    initKeyEventListen() {
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN, (e) => {
            // console.log('keydown: ',e)
            let keyCode = e.keyCode
            let key = codeToKey(keyCode)
            // console.log('down:', key)
            this.excuteAction(key)
        }, this)

        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP, (e) => {
            // console.log('keydown: ',e)
            let keyCode = e.keyCode
            let key = codeToKey(keyCode)
            if (key == 'a' || key == 'd') {
                this.excuteAction('stop')
            }
            // console.log('up:', key)
        }, this)
    },

    // 根据 key 执行对应的动作
    excuteAction(key) {
        if (!key) {
            return
        }

        let table = {
            'a': this.moveLeft.bind(this),
            'd': this.moveRight.bind(this),
            'stop': this.stopMove.bind(this),
            'j': this.playerJump.bind(this)
        }

        // console.log(table, key, table[key])

        table[key]()
    },

    // 向左移动
    moveLeft() {
        this.isStop = false
        this.isLeft = true
    },

    // 向右移动
    moveRight() {
        this.isStop = false
        this.isLeft = false
    },

    // 停止左右移动
    stopMove() {
        this.isStop = true
        let rigidbody = this.player.getComponent(cc.RigidBody)
        let velocity = rigidbody.linearVelocity
        let direction = this.isLeft ? -1 : 1
        velocity.x = 0
        rigidbody.linearVelocity = velocity
    },

    // 跳跃
    playerJump() {
        // console.log(e.getLocation(), this.canvas.width)
        if (this.player) {
            let rigidbody = this.player.getComponent(cc.RigidBody)
            let velocity = rigidbody.linearVelocity
            velocity.y = this.verticalSpeed
            rigidbody.linearVelocity = velocity
            // console.log('jump', this.verticalSpeed)
        }

    },

    updateMove() {
        if (this.player) {
            try {
                if (!this.isStop) {
                    let rigidbody = this.player.getComponent(cc.RigidBody)
                    let velocity = rigidbody.linearVelocity
                    let direction = this.isLeft ? -1 : 1
                    this.player.scaleX = direction
                    velocity.x = this.horizontalSpeed * direction
                    rigidbody.linearVelocity = velocity
                }

            } catch (e) {
                console.error('err player controller', e)
            }
        }
    },

    update(dt) {
        this.updateMove()
        // console.log(this.isStop)
    },
})

