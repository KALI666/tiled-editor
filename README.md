# 瓦片地图编辑器

## 在线体验
+ http://jiankesword.gitee.io/build-editor/

## 运行示例 
![image](https://gitee.com/jiankesword/image-layer/raw/master/mapedit/mapEdit1.gif)
![image](https://gitee.com/jiankesword/image-layer/raw/master/mapedit/mapEdit0.gif)


## 项目环境和配置
+ cocos creator 2.2.1
+ JavaScript

## 项目说明
+ TODO: ...

## 配色
+ http://www.flatuicolorpicker.com/blue-rgb-color-model

## 重构已完成
+ 可以编辑任意大小的地图
+ 地图块的配置组件化，可以自定义不同功能的地图块 （比如怪物，弹簧，收集物等等，以后可能会有例子）


